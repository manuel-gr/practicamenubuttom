package com.example.appmenubutton.database

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenubutton.R
import com.example.appmenubutton.databinding.AlumnoItemBinding
import com.squareup.picasso.Picasso
import java.io.File
import android.widget.Toast

class MiAdaptador(
    private var alumnos: List<AlumnoLista>,
    private val context: Context,
    private val listener: OnItemClickListener
) : RecyclerView.Adapter<MiAdaptador.AlumnoViewHolder>(), Filterable {

    private var alumnosFiltrados: List<AlumnoLista> = alumnos

    interface OnItemClickListener {
        fun onItemClick(alumno: AlumnoLista)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlumnoViewHolder {
        val binding = AlumnoItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AlumnoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AlumnoViewHolder, position: Int) {
        val alumno = alumnosFiltrados[position]
        holder.bind(alumno)
    }

    override fun getItemCount(): Int {
        return alumnosFiltrados.size
    }

    inner class AlumnoViewHolder(private val binding: AlumnoItemBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        fun bind(alumno: AlumnoLista) {
            binding.txtAlumnoNombre.text = alumno.nombre
            binding.txtCarrera.text = alumno.especilidad
            binding.txtMatricula.text = alumno.matricula

            val imagePath = alumno.foto
            if (imagePath != null) {
                val imageFile = File(imagePath)
                if (imageFile.exists()) {
                    Picasso.get().load(imageFile).into(binding.foto)
                } else {
                    binding.foto.setImageResource(R.mipmap.ic_launcher) // Imagen predeterminada
                }
            } else {
                binding.foto.setImageResource(R.mipmap.ic_launcher) // Imagen predeterminada
            }
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(alumnosFiltrados[position])
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint?.toString()?.replace("\\s+".toRegex(), "") ?: ""
                alumnosFiltrados = if (charString.isEmpty()) alumnos else {
                    val filteredList = ArrayList<AlumnoLista>()
                    alumnos.filter {
                        it.nombre.replace("\\s+".toRegex(), "").contains(charString, true) ||
                                it.matricula.replace("\\s+".toRegex(), "").contains(charString, true)
                    }.forEach { filteredList.add(it) }
                    filteredList
                }
                return FilterResults().apply { values = alumnosFiltrados }
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                alumnosFiltrados = if (results?.values == null)
                    ArrayList()
                else
                    results.values as List<AlumnoLista>
                notifyDataSetChanged()
            }
        }
    }
}

