package com.example.appmenubutton

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
//import dbFragment

class MainActivity : AppCompatActivity() {
    private lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        bottomNavigationView = findViewById(R.id.btnNavegador)
        bottomNavigationView.setOnItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.btnhome -> {
                    cambiarFrame(HomeFragment())
                    true
                }
                R.id.btnLista -> {
                    cambiarFrame(ListFragment())
                    true
                }
                R.id.btnDB -> {
                    cambiarFrame(dbFragment())
                    true
                }
                R.id.btnSalir -> {
                    cambiarFrame(SalirFragment())
                    true
                }
                else -> false
            }
        }
        if (savedInstanceState == null) {
            cambiarFrame(HomeFragment())
            bottomNavigationView.selectedItemId = R.id.btnhome
        }


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    private fun cambiarFrame(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.frm_Contenedor, fragment).commit()
    }
}
